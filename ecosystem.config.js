module.exports = {
	apps: [{
		name: "api",
		script: "./index.js",
		watch: true,
		env_local: {
			"NODE_ENV": "local",
			"API_DESCRIPTION": "Running in development mode"
		},
		env_production: {
			"NODE_ENV": "production",
			"API_DESCRIPTION": "Running in production mode"
		}
	}]
};
