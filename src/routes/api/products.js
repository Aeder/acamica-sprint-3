const express = require('express');
var router = express.Router();
const { models } = require('../../db/connection')
const middleware = require('../../middleware/index');
const redisClient = require('../../cache/index')


router.get('/', async (req, res) => {

    try {
        redisClient.get('products', async (error, reply) => {
            if (reply) {
                console.log('got cached products:' + reply);
                res.status(200).json(JSON.parse(reply));
            } else {
                const products = await models.Product.findAll()

                redisClient.set('products', JSON.stringify(products), 'EX', 3600)

                res.status(200).json(products);
            }
        });

    } catch (e) {
        console.log(e);
        res.status(500);
    }

})

router.post('/', middleware.isAdmin, async (req, res) => {
    const product_data = req.body;
    console.log(req.body);

    const new_product = await models.Product.create({
        name: product_data.name,
        category: product_data.category,
        price: product_data.price
    });

    res.send(new_product);

})

router.patch('/:id', middleware.isAdmin, async (req, res) => {
    const product_new_data = req.body;

    updated_product = await models.Product.findOne({
        where: {
            id: req.params.id
        }
    })

    console.log(`Product found: ${updated_product}`);

    Object.keys(product_new_data).forEach(key => {
        updated_product[key] = product_new_data[key];
    })
    await updated_product.save();
    res.json(updated_product);
})

router.delete('/:id', middleware.isAdmin, async (req, res) => {

    deleted_product = await models.Product.findOne({
        where: {
            id: req.params.id
        }
    });

    await deleted_product.destroy();

    res.send("Producto borrado");
});

module.exports = router;