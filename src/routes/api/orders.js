const express = require('express');
const router = express.Router();
const { models } = require('../../db/connection');
const middleware = require('../../middleware/index');


router.get('/', middleware.isAdmin, async (req, res) => {

  orders_list = await models.Order.findAll({include: [{ model: models.Product, through: { attributes: ['amount'] } }]});
  res.json(orders_list);

})

router.get('/history/', middleware.isLogged, async (req, res) => {

  const user_id = req.user_id;

  let user_orders = await models.Order.findAll({
    where: {
      user_id: user_id,
    },
    include: [{ model: models.Product, through: { attributes: ['amount'] } }]
  })

  res.json(user_orders)
})

router.post('/', middleware.isLogged, async (req, res) => {
  const order_data = req.body;
  const user_id = req.user_id;
  let total_price = 0;

  try {
    await models.Order.create({
      user_id: user_id,
      address: order_data.address,
      payment_method_id: order_data.payment_method
    }).then((created_order) => {
      order_data.products.forEach(async (product) => {
        await models.Product.findOne({
          where: {
            id: product.id
          }
        }).then(async (result) => {
          const product_instance = result;
          total_price = product_instance.price * product.amount;
          await created_order.addProduct(product_instance, { through: { amount: product.amount } }).then(async () => {
            created_order.price += total_price
            await created_order.save().then((result) => {
              res.status(201).json(result) // Reload with attributtes to get product list?
            }
            )
          });
        })
      });
    })
  } catch (Error) {
    console.error("Error: " + Error)
    res.status(500)
  }
})

router.patch('/:id', middleware.isAdmin, async (req, res) => {
  const order_new_data = req.body.data;

  console.log(order_new_data)

  try {
    await models.Order.findOne({ where: { id: req.params.id } })
      .then(async (order_instance) => {

        if (order_instance == null) {
          res.status(404).json(order_instance);
        }

        console.log(`Order found: ${order_instance}`);

        Object.keys(order_new_data).forEach(key => {
          order_instance[key] = order_new_data[key];
        })

        await order_instance.save().then((result) => {
          res.json(result);
        });

      })
  } catch (e) {
    console.log(e)
    res.status(500);
  }

})

module.exports = router;