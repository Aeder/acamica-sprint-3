const express = require('express');
var router = express.Router();
const { models } = require('../../db/connection');
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET

router.post('/', async (req, res) => {
    const new_user_data = req.body;

    try {
        await models.User.create({
            username: new_user_data.username,
            password: new_user_data.password,
            admin: false,
            full_name: new_user_data.full_name,
            phone: new_user_data.phone,
            email: new_user_data.email,
            address: new_user_data.address
        }).then((result)=>{
            if (result) {
                res.status(200).json(result);
            } else {
                res.status(500).json("El email seleccionado ya esta en uso")
            }
        })
    } catch (e) {
        console.log("Couldn't create new user: " + e);
        res.status(500).json("No se pudo crear el usuario")
    }
})

router.post('/login', async (req, res) => {
    
    const { request_email, request_password } = req.body;
    console.log(req.body);

    let requested_user;

    try {
        requested_user = await models.User.findOne({
            where: {
                email: request_email
            }
        });
        console.log(requested_user)
    
    } catch (e) {
        console.log("Couldn't find the requested user:" + e)
    }

    if (requested_user == null) {
        res.status(404).send("No existe el usuario");
    } else {
        if (request_password != requested_user.password) {
            res.status(403).send("Contraseña incorrecta");
        } else {
            user_info = { user_id: requested_user.id};
            token = jwt.sign(user_info, jwtSecret);
            res.status(200).json({ token: token });
        }
    }

});

module.exports = router;