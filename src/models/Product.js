const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    sequelize.define('Product', {
        name: {
            type: DataTypes.STRING
        },
        category: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.DOUBLE
        }
    })
}