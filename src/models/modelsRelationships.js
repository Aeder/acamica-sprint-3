function createModelsRelationships(sequelize) {
    const { Order, Product, User, OrderProducts, PaymentMethod } = sequelize.models;

    User.hasMany(Order, {
        foreignKey: 'user_id',
        targetKey: 'id'
    })
    Order.belongsTo(User, {
        foreignKey: 'user_id',
        targetKey: 'id'
    });
    Order.belongsTo(PaymentMethod, {
        foreignKey: 'payment_method_id',
        targetKey: 'id'
    });
    PaymentMethod.hasMany(Order, {
        foreignKey: 'payment_method_id',
        targetKey: 'id'
    });
    Order.belongsToMany(Product, { through: OrderProducts, foreignKey: 'order_id'});
    Product.belongsToMany(Order, { through: OrderProducts, foreignKey: 'product_id'});

}

module.exports = (sequelize) => { createModelsRelationships(sequelize) };