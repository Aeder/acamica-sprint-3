const dotenv = require('dotenv').config();
const express = require("express");
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

const swaggerOptions = {
    swaggerDefinition: {
        openapi: "3.0.0",
        components: {
            securitySchemes: {
                BearerAuth: {
                    type: 'http',
                    scheme: 'bearer',
                    bearerFormat: 'JWT'
                }
            }
        },
        info: {
            title: 'Acamica Sprint 2 API',
            version: '2.0.2'
        }
    },
    apis: [
        './docs/product.yaml',
        './docs/user.yaml',
        './docs/order.yaml',
        './docs/payment_method.yaml'
    ],
};

const swaggerSpec = swaggerJSDoc(swaggerOptions);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/api', require('./src/routes/api'));

app.listen(5000, () => { console.log("Corriendo en http://127.0.0.1:5000/") });

module.exports = app;