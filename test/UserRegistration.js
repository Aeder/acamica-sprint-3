app = require("../index.js")
db = require("../src/db/connection")
let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:5000';

beforeEach(async () => {
    await db.sync({force: true});
})

describe('Feature tests for /users route: ', function() {
    this.timeout(20000);
    it('it should create a new user', (done) => {
        chai.request(url)
            .post('/api/users')
            .send({
                username: "testUser",
                full_name: "test user",
                phone: "12344567",
                email: "test@example.com",
                address: "testAddress 123",
                password: "testpassword"
            })
            .end((err, res) => {
                console.log(res.body)
                expect(res).to.have.status(200);
                done();
            });
    });
});
