# acamica-sprint-3

Proyecto para el segundo sprint de Acamica

# Instalación en Linux (Fedora)

1. Asegurarse de tener git, redis y npm instalados:

```
sudo dnf install npm git redis
```

2. Ejecutar git clone en la carpeta donde queremos descargar el programa

```
cd mi-carpeta
git clone https://gitlab.com/Aeder/acamica-sprint-3.git
```

3. Instalar las librerias necesarias
```
cd acamica-sprint-3
npm install
```

# Configuracion

1. Crear un archivo .env utilizando el contenido de .env.example como ejemplo

2. Completar las variables:

* DB_HOST : Dirección IP o URL del servidor de base de datos MariaDB
* DB_USER : Usuario de la base de datos
* DB_PASS : Contraseña del usuario de base de datos
* DB_NAME : Nombre de la base de datos a usar 
* JWT_SECRET : Secreto para firmar los tokens JWT. Preferiblemente debe ser largo y difícil de predecir.
* REDIS_HOST : Direccion IP o URL del servidor de cache Redis.
* REDIS_PORT : Puerto empleado para conectarse al servidor Redis.

# Correr el programa

1. Asegurarse que MariaDB y Redis esten funcionando. Para ello ejecutar los siguientes comandos (En sistemas linux con systemd)

```
sudo systemctl start mariadb
sudo systemctl start redis
```

2. Luego, para correr el programa ejecutar el siguiente comando en consola

```
npm run start
```

# Pruebas

1. Si se desea ejecutar las prueba se debe usar el siguiente commando

```
npm run test
```